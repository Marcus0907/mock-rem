const express = require('express');
const app = express();
const cors = require('cors')

const db = require('./db.json');

const port = 5001;

app.use(cors())

app.use(express.json());


app.get('/contactList', (req, res) => {
  res.send({result: db.contactList} )
})

app.post('/contactList', (req, res) => {
  let newList = req.body
  newList = {
    id: db.contactList.length,
    ...newList,
    status: "Active",
    totalContacts: 0,
    totalEmailSent: 0,
    totalEmailSentSuccess: 0,
    totalSmsMmsSent: 0,
    totalSmsMmsSentSuccess: 0,
  }

  db.contacts.push({
    id: db.contactList.length,
    list: []
  })

  db.contactList.push(newList);
  res.send({result:newList})
})

app.put('/contactList/:id', (req, res) => {
  let contactList = db.contactList.find( el => 
    el.id == Number(req.params.id)
  )

  db.contactList = db.contactList.filter( el => el.id != req.params.id)

  contactList = {
    ...contactList,
    ...req.body,
  }

  db.contactList.push(contactList)
  
  res.send({result:db.contactList})
})

app.delete('/contactList/:id', (req, res) => {
  db.contactList = db.contactList.filter( el => el.id != req.params.id)
  db.contacts = db.contacts.filter(el => el.id != req.params.id)
  res.send({result:db.contactList})
})

app.get('/contacts/:id', (req, res) => {
  const list = db.contacts.filter(v => v.id == req.params.id)
  res.send({result:list[0].list})
})
app.post('/contacts', (req, res) => {
  let newList = [];
  db.contacts.forEach(con => {
    if(con.id == req.body.idContactList){
      con.list.push({...req.body, id:con.list.length})
      newList = con.list
    }
  })
  
  res.send({result:newList})
})
app.put('/contacts', (req, res) => {
  let newList = [];
  db.contacts.forEach(con => {
    if(con.id == req.body.idContactList){
      con.list.forEach((con2,index,array) => {
        if(con2.id == req.body.id){
        
          array[index] = {...req.body}
        }
      })
      newList = con.list
    }
  })
  
  res.send({result:newList})
})
app.delete('/contacts/:idContactList/:id', (req, res) => {
  let newList = [];
  db.contacts.forEach((con, index, array) => {
    if(con.id == req.params.idContactList){
      newList = con.list.filter(con2 => con2.id != req.params.id)
      array[index].list = con.list.filter(con2 => con2.id != req.params.id)
    }
  })
  
  res.send({result:newList})
})

//template

app.get('/v1/campaign-template', (req, res) => {

  res.send({result:db.templateList})
})

app.listen(port,()=>{
  console.log(`Listen on port ${port}`);
})

